var notification = null;
chrome.extension.onMessage.addListener(onMessage);

function onMessage(request, sender, sendResponse) {
	console.log(sender.tab ? "from a content script:" + sender.tab.url
			: "from the extension");

	switch (request.action) {
	case "notify":

		if (notification == null) {
			// Or create an HTML notification:
			notification = webkitNotifications.createHTMLNotification(
			  'notification.html'
			);
			
			notification.show();
		}
		
		if (request.message == "isPublic") {
			notification = null;
		}
		
		
		
		chrome.extension.getViews({type:"notification"}).forEach(function(win) {
			  win.updateStatus(request);
		});

		break;

	default:
		break;
	}

};

function doOpenLink(data) {
	console.log('doOpenLink', data);
	chrome.tabs.create({
		url : data.url
	});
}