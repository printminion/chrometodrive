var notification = null;
var url = null;


function updateStatus(request, alternateLink) {
	console.log('updateStatus', request, request.action, request.message);
	
	var message = request.message;
	
	if (request.message == "uploadedToGDrive") {
		url = request.url;
	} else if (request.message == "isPublic") {
		message = request.message + ' done';
	}
	
	var statusObj = document.getElementById('status');
	statusObj.innerHTML = message;
};

window.onclick = function() {
	
	chrome.extension.getBackgroundPage().doOpenLink({'url': url});
	window.close();
};