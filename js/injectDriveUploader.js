console.log('injectDriveUploader');

var FILE_TO_DOWLOAD = '';
var FILE_BASE64 = '';
var FILE_DATA_MIME_TYPE = '';
var ACCESS_TOKEN = '';


chrome.extension.onMessage.addListener(
  function(request, sender, sendResponse) {
    console.log(sender.tab ?
                "from a content script:" + sender.tab.url :
                "from the extension");
    
    if (request.action == "download") {
    	
    	chrome.extension.sendMessage({action: "notify", message: "startDownload"}, function(response) {
    		  console.log(response);
    	});
    	
    	
    	downloadFile(request.url, function(data, mimeType) {
    		FILE_TO_DOWLOAD = request.url;
    		ACCESS_TOKEN = request.accessToken;
    		
    		var FILE_DESCRIPTION = 'File:' + request.url + '\nParent:' + document.location;
    		
    		var FILE_NAME = FILE_TO_DOWLOAD.split('/');
    		FILE_NAME = FILE_NAME[FILE_NAME.length - 1];
    		FILE_BASE64 = data;
    		
    		/*
    		 * try to get mime type
    		 */
    		if (!mimeType) {
    			mimeType = getMimeTypeByExtension(FILE_NAME);
    		}
    		
    		chrome.extension.sendMessage({action: "notify", message: "downloaded"}, function(response) {
      		  console.log(response);
    		});
      		
    		chrome.extension.sendMessage({action: "notify", message: "uploadingToGDrive"}, function(response) {
      		  console.log(response);
    		});
      	
    		insertFileOAuth(FILE_NAME, FILE_BASE64, null, mimeType, ACCESS_TOKEN, FILE_DESCRIPTION, function(data) {
    			
    			chrome.extension.sendMessage({action: "notify", message: "uploadedToGDrive", url: data.alternateLink}, function(response) {
    	    		  console.log(response);
    	    	});
    	    	
    			
    			chrome.extension.sendMessage({action: "notify", message: "makingPublic"}, function(response) {
  	    		  console.log(response);
    			});
    			doChangePermissions(data.id, 'anyone', ACCESS_TOKEN, function(data){
    				console.log('doChangePermissionsCallback', data);
    		    	
	    			chrome.extension.sendMessage({action: "notify", message: "isPublic"}, function(response) {
	      	    		  console.log(response);
	      	    	});

    			});

    			
    		});

    	});
    }
    
});


function getMimeTypeByExtension(url){
	var extToMimes = {
			'img': 'image/jpeg'
		    , 'png': 'image/png'
		    , 'jpg': 'image/jpeg'
		    , 'jpeg': 'image/jpeg'
		    , 'gif': 'image/gif'
		    };

	var ext = url.split('.');
	   ext = ext[ext.length - 1];
	
	
	ext = ext.toLowerCase();
	
	if (extToMimes.hasOwnProperty(ext)) {
        return extToMimes[ext];
	}
    
     return false;
}

function downloadFile(url, callback) {

	var xhr = new XMLHttpRequest();
	xhr.open('GET', url, true);

	xhr.responseType = 'arraybuffer';

	xhr.onload = function(e) {
		if (this.status == 200) {
			var uInt8Array = new Uint8Array(this.response);
			var i = uInt8Array.length;
			var binaryString = new Array(i);
			while (i--) {
				binaryString[i] = String.fromCharCode(uInt8Array[i]);
			}
			var data = binaryString.join('');

			var base64 = window.btoa(data);

			callback(base64, this.getResponseHeader('content-type'));
		}
	};

	xhr.send();
}



function insertFileOAuth(fileDataName, fileBase64Data, fileDataType, contentMimeType, oauthToken, fileDescription, callback) {
	console.log('insertFileOAuth', fileDataName, 'fileBase64Data', fileDataType, contentMimeType, oauthToken, fileDescription, callback);
	const boundary = '-------314159265358979323846';
	const delimiter = "\r\n--" + boundary + "\r\n";
	const close_delim = "\r\n--" + boundary + "--";

  var contentType = fileDataType || 'application/octet-stream';
  var metadata = {
    'description': fileDescription
    , 'title': fileDataName
    , 'mimeType': contentMimeType
    , 'labels.restricted': false
    , 'labels.starred': true
  };

//  var base64Data = btoa(reader.result);
  var multipartRequestBody =
      delimiter +
      'Content-Type: application/json\r\n\r\n' +
      JSON.stringify(metadata) +
      delimiter +
      'Content-Type: ' + contentType + '\r\n' +
      'Content-Transfer-Encoding: base64\r\n' +
      '\r\n' +
      fileBase64Data +
      close_delim;
  
  var xhr = new XMLHttpRequest();
  xhr.open('POST', 'https://www.googleapis.com/upload/drive/v2/files', false);
  xhr.setRequestHeader('Authorization', 'OAuth ' + oauthToken);
  xhr.setRequestHeader('Content-Type', 'multipart/mixed; boundary="' + boundary + '"');
  
	xhr.onload = function(e) {
		if (this.status == 200) {
			var response = JSON.parse(this.response);
			console.log('done', response);
			
			callback(response);
		}
	};

  
  xhr.send(multipartRequestBody);
};
  

/**
 * 
 * @url https://developers.google.com/drive/v2/reference/permissions/insert
 * 
 * @param fileid
 * @param authToken
 * @param callback
 */
function doChangePermissions(fileId, type, authToken, callback) {
	console.log('doChangePermissions', fileId, authToken, callback);
  
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'https://www.googleapis.com/drive/v2/files/' + fileId + '/permissions', false);
    xhr.setRequestHeader('Authorization', 'OAuth ' + authToken);
    xhr.setRequestHeader('Content-Type', 'application/json');
    
    xhr.onload = function(e) {
		if (this.status == 200) {
			var response = JSON.parse(this.response);
			console.log('done', response);
			
			callback(response);
		}
	};

    
    xhr.send(JSON.stringify({"role": "reader", "type": type, "withLink": true}));
    
};

  
